# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**number** | **int** |  | [optional] 
**decsribe** | **string** |  | [optional] 
**genre** | **string** |  | [optional] 
**price** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**date** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


