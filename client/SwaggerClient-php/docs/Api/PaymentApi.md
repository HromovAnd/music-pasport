# Swagger\Client\PaymentApi

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentLicense**](PaymentApi.md#paymentLicense) | **POST** /payment | payment license
[**setCard**](PaymentApi.md#setCard) | **POST** /payment/setCard | Set card user


# **paymentLicense**
> \Swagger\Client\Model\Cheque paymentLicense($body)

payment license

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Cheque(); // \Swagger\Client\Model\Cheque | 

try {
    $result = $apiInstance->paymentLicense($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentLicense: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Cheque**](../Model/Cheque.md)|  |

### Return type

[**\Swagger\Client\Model\Cheque**](../Model/Cheque.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setCard**
> \Swagger\Client\Model\Card setCard($body)

Set card user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Card(); // \Swagger\Client\Model\Card | 

try {
    $result = $apiInstance->setCard($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->setCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Card**](../Model/Card.md)|  |

### Return type

[**\Swagger\Client\Model\Card**](../Model/Card.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

