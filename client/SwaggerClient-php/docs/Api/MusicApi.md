# Swagger\Client\MusicApi

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMusic**](MusicApi.md#createMusic) | **POST** /music | Add a new music
[**delMusic**](MusicApi.md#delMusic) | **DELETE** /music/delete/{musicId} | Deletes a music
[**getMusicByAlbum**](MusicApi.md#getMusicByAlbum) | **GET** /music/get/{albumId} | Finds music by album


# **createMusic**
> \Swagger\Client\Model\Music createMusic($body)

Add a new music

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\MusicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Music(); // \Swagger\Client\Model\Music | 

try {
    $result = $apiInstance->createMusic($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MusicApi->createMusic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Music**](../Model/Music.md)|  |

### Return type

[**\Swagger\Client\Model\Music**](../Model/Music.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **delMusic**
> delMusic($music_id, $api_key)

Deletes a music

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\MusicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$music_id = 789; // int | 
$api_key = "api_key_example"; // string | 

try {
    $apiInstance->delMusic($music_id, $api_key);
} catch (Exception $e) {
    echo 'Exception when calling MusicApi->delMusic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **music_id** | **int**|  |
 **api_key** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMusicByAlbum**
> \Swagger\Client\Model\Music[] getMusicByAlbum($album_id)

Finds music by album

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\MusicApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$album_id = 56; // int | 

try {
    $result = $apiInstance->getMusicByAlbum($album_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MusicApi->getMusicByAlbum: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **album_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\Music[]**](../Model/Music.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

