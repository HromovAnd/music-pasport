# Swagger\Client\PublicationApi

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPublication**](PublicationApi.md#createPublication) | **POST** /publication | Add a new publication
[**getPublicationsByUserId**](PublicationApi.md#getPublicationsByUserId) | **GET** /publication/get/{userId} | Finds publications by userId


# **createPublication**
> \Swagger\Client\Model\Publication createPublication($body)

Add a new publication

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PublicationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Publication(); // \Swagger\Client\Model\Publication | 

try {
    $result = $apiInstance->createPublication($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PublicationApi->createPublication: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Publication**](../Model/Publication.md)|  |

### Return type

[**\Swagger\Client\Model\Publication**](../Model/Publication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPublicationsByUserId**
> \Swagger\Client\Model\Publication[] getPublicationsByUserId($user_id)

Finds publications by userId

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PublicationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$user_id = 56; // int | 

try {
    $result = $apiInstance->getPublicationsByUserId($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PublicationApi->getPublicationsByUserId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\Publication[]**](../Model/Publication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

