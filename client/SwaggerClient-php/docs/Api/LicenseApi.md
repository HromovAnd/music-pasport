# Swagger\Client\LicenseApi

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLicense**](LicenseApi.md#createLicense) | **POST** /license | Add a new license
[**getLicensesByUserId**](LicenseApi.md#getLicensesByUserId) | **GET** /license/get/{userId} | Finds licenses by userId


# **createLicense**
> \Swagger\Client\Model\License createLicense($body)

Add a new license

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LicenseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\License(); // \Swagger\Client\Model\License | 

try {
    $result = $apiInstance->createLicense($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LicenseApi->createLicense: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\License**](../Model/License.md)|  |

### Return type

[**\Swagger\Client\Model\License**](../Model/License.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLicensesByUserId**
> \Swagger\Client\Model\License[] getLicensesByUserId($user_id)

Finds licenses by userId

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LicenseApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$user_id = 56; // int | 

try {
    $result = $apiInstance->getLicensesByUserId($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LicenseApi->getLicensesByUserId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\License[]**](../Model/License.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

