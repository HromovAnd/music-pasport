# Swagger\Client\AlbumApi

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAlbum**](AlbumApi.md#createAlbum) | **POST** /ablum | Add a new album
[**delAlbum**](AlbumApi.md#delAlbum) | **DELETE** /aibum/delete/{albumId} | Deletes a album
[**getAlbumsByCategory**](AlbumApi.md#getAlbumsByCategory) | **GET** /album/get | Finds albums by category


# **createAlbum**
> \Swagger\Client\Model\Album createAlbum($body)

Add a new album

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AlbumApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Album(); // \Swagger\Client\Model\Album | 

try {
    $result = $apiInstance->createAlbum($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AlbumApi->createAlbum: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Album**](../Model/Album.md)|  |

### Return type

[**\Swagger\Client\Model\Album**](../Model/Album.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **delAlbum**
> delAlbum($album_id, $api_key)

Deletes a album

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AlbumApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$album_id = 789; // int | 
$api_key = "api_key_example"; // string | 

try {
    $apiInstance->delAlbum($album_id, $api_key);
} catch (Exception $e) {
    echo 'Exception when calling AlbumApi->delAlbum: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **album_id** | **int**|  |
 **api_key** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAlbumsByCategory**
> \Swagger\Client\Model\Album[] getAlbumsByCategory($category)

Finds albums by category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AlbumApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$category = "category_example"; // string | 

try {
    $result = $apiInstance->getAlbumsByCategory($category);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AlbumApi->getAlbumsByCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category** | **string**|  |

### Return type

[**\Swagger\Client\Model\Album[]**](../Model/Album.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

