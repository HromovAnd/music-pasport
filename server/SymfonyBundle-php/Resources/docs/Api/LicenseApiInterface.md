# Swagger\Server\Api\LicenseApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLicense**](LicenseApiInterface.md#createLicense) | **POST** /license | Add a new license
[**getLicensesByUserId**](LicenseApiInterface.md#getLicensesByUserId) | **GET** /license/get/{userId} | Finds licenses by userId


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.license:
        class: Acme\MyBundle\Api\LicenseApi
        tags:
            - { name: "swagger_server.api", api: "license" }
    # ...
```

## **createLicense**
> Swagger\Server\Model\License createLicense($body)

Add a new license

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/LicenseApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\LicenseApiInterface;

class LicenseApi implements LicenseApiInterface
{

    // ...

    /**
     * Implementation of LicenseApiInterface#createLicense
     */
    public function createLicense(License $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\License**](../Model/License.md)|  |

### Return type

[**Swagger\Server\Model\License**](../Model/License.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getLicensesByUserId**
> Swagger\Server\Model\License getLicensesByUserId($userId)

Finds licenses by userId

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/LicenseApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\LicenseApiInterface;

class LicenseApi implements LicenseApiInterface
{

    // ...

    /**
     * Implementation of LicenseApiInterface#getLicensesByUserId
     */
    public function getLicensesByUserId($userId)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **int**|  |

### Return type

[**Swagger\Server\Model\License**](../Model/License.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

