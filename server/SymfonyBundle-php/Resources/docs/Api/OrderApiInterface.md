# Swagger\Server\Api\OrderApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrder**](OrderApiInterface.md#createOrder) | **POST** /order | Add a new order
[**getOrdersByUserId**](OrderApiInterface.md#getOrdersByUserId) | **GET** /order/get/{userId} | Finds orders by userId


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.order:
        class: Acme\MyBundle\Api\OrderApi
        tags:
            - { name: "swagger_server.api", api: "order" }
    # ...
```

## **createOrder**
> Swagger\Server\Model\Order createOrder($body)

Add a new order

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/OrderApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\OrderApiInterface;

class OrderApi implements OrderApiInterface
{

    // ...

    /**
     * Implementation of OrderApiInterface#createOrder
     */
    public function createOrder(Order $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Order**](../Model/Order.md)|  |

### Return type

[**Swagger\Server\Model\Order**](../Model/Order.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getOrdersByUserId**
> Swagger\Server\Model\Order getOrdersByUserId($userId)

Finds orders by userId

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/OrderApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\OrderApiInterface;

class OrderApi implements OrderApiInterface
{

    // ...

    /**
     * Implementation of OrderApiInterface#getOrdersByUserId
     */
    public function getOrdersByUserId($userId)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **int**|  |

### Return type

[**Swagger\Server\Model\Order**](../Model/Order.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

