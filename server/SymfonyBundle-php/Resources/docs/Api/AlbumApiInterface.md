# Swagger\Server\Api\AlbumApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAlbum**](AlbumApiInterface.md#createAlbum) | **POST** /ablum | Add a new album
[**delAlbum**](AlbumApiInterface.md#delAlbum) | **DELETE** /aibum/delete/{albumId} | Deletes a album
[**getAlbumsByCategory**](AlbumApiInterface.md#getAlbumsByCategory) | **GET** /album/get | Finds albums by category


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.album:
        class: Acme\MyBundle\Api\AlbumApi
        tags:
            - { name: "swagger_server.api", api: "album" }
    # ...
```

## **createAlbum**
> Swagger\Server\Model\Album createAlbum($body)

Add a new album

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/AlbumApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\AlbumApiInterface;

class AlbumApi implements AlbumApiInterface
{

    // ...

    /**
     * Implementation of AlbumApiInterface#createAlbum
     */
    public function createAlbum(Album $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Album**](../Model/Album.md)|  |

### Return type

[**Swagger\Server\Model\Album**](../Model/Album.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **delAlbum**
> delAlbum($albumId, $apiKey)

Deletes a album

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/AlbumApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\AlbumApiInterface;

class AlbumApi implements AlbumApiInterface
{

    // ...

    /**
     * Implementation of AlbumApiInterface#delAlbum
     */
    public function delAlbum($albumId, $apiKey = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **albumId** | **int**|  |
 **apiKey** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getAlbumsByCategory**
> Swagger\Server\Model\Album getAlbumsByCategory($category)

Finds albums by category

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/AlbumApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\AlbumApiInterface;

class AlbumApi implements AlbumApiInterface
{

    // ...

    /**
     * Implementation of AlbumApiInterface#getAlbumsByCategory
     */
    public function getAlbumsByCategory($category)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category** | **string**|  |

### Return type

[**Swagger\Server\Model\Album**](../Model/Album.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

