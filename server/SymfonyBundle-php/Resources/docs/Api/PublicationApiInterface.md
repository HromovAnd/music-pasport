# Swagger\Server\Api\PublicationApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPublication**](PublicationApiInterface.md#createPublication) | **POST** /publication | Add a new publication
[**getPublicationsByUserId**](PublicationApiInterface.md#getPublicationsByUserId) | **GET** /publication/get/{userId} | Finds publications by userId


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.publication:
        class: Acme\MyBundle\Api\PublicationApi
        tags:
            - { name: "swagger_server.api", api: "publication" }
    # ...
```

## **createPublication**
> Swagger\Server\Model\Publication createPublication($body)

Add a new publication

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PublicationApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PublicationApiInterface;

class PublicationApi implements PublicationApiInterface
{

    // ...

    /**
     * Implementation of PublicationApiInterface#createPublication
     */
    public function createPublication(Publication $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Publication**](../Model/Publication.md)|  |

### Return type

[**Swagger\Server\Model\Publication**](../Model/Publication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getPublicationsByUserId**
> Swagger\Server\Model\Publication getPublicationsByUserId($userId)

Finds publications by userId

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PublicationApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PublicationApiInterface;

class PublicationApi implements PublicationApiInterface
{

    // ...

    /**
     * Implementation of PublicationApiInterface#getPublicationsByUserId
     */
    public function getPublicationsByUserId($userId)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **int**|  |

### Return type

[**Swagger\Server\Model\Publication**](../Model/Publication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

