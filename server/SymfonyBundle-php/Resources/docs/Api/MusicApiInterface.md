# Swagger\Server\Api\MusicApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMusic**](MusicApiInterface.md#createMusic) | **POST** /music | Add a new music
[**delMusic**](MusicApiInterface.md#delMusic) | **DELETE** /music/delete/{musicId} | Deletes a music
[**getMusicByAlbum**](MusicApiInterface.md#getMusicByAlbum) | **GET** /music/get/{albumId} | Finds music by album


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.music:
        class: Acme\MyBundle\Api\MusicApi
        tags:
            - { name: "swagger_server.api", api: "music" }
    # ...
```

## **createMusic**
> Swagger\Server\Model\Music createMusic($body)

Add a new music

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/MusicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\MusicApiInterface;

class MusicApi implements MusicApiInterface
{

    // ...

    /**
     * Implementation of MusicApiInterface#createMusic
     */
    public function createMusic(Music $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Music**](../Model/Music.md)|  |

### Return type

[**Swagger\Server\Model\Music**](../Model/Music.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **delMusic**
> delMusic($musicId, $apiKey)

Deletes a music

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/MusicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\MusicApiInterface;

class MusicApi implements MusicApiInterface
{

    // ...

    /**
     * Implementation of MusicApiInterface#delMusic
     */
    public function delMusic($musicId, $apiKey = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **musicId** | **int**|  |
 **apiKey** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getMusicByAlbum**
> Swagger\Server\Model\Music getMusicByAlbum($albumId)

Finds music by album

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/MusicApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\MusicApiInterface;

class MusicApi implements MusicApiInterface
{

    // ...

    /**
     * Implementation of MusicApiInterface#getMusicByAlbum
     */
    public function getMusicByAlbum($albumId)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **albumId** | **int**|  |

### Return type

[**Swagger\Server\Model\Music**](../Model/Music.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

