# Swagger\Server\Api\CategoryApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCategory**](CategoryApiInterface.md#createCategory) | **POST** /category | Add a new category
[**delCategory**](CategoryApiInterface.md#delCategory) | **DELETE** /category/delete/{categoryId} | Deletes a category
[**getCategoriesByFilter**](CategoryApiInterface.md#getCategoriesByFilter) | **GET** /category/findByfilter | Finds categories by filter


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.category:
        class: Acme\MyBundle\Api\CategoryApi
        tags:
            - { name: "swagger_server.api", api: "category" }
    # ...
```

## **createCategory**
> Swagger\Server\Model\Category createCategory($body)

Add a new category

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CategoryApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CategoryApiInterface;

class CategoryApi implements CategoryApiInterface
{

    // ...

    /**
     * Implementation of CategoryApiInterface#createCategory
     */
    public function createCategory(Category $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Category**](../Model/Category.md)|  |

### Return type

[**Swagger\Server\Model\Category**](../Model/Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **delCategory**
> delCategory($categoryId, $apiKey)

Deletes a category

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CategoryApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CategoryApiInterface;

class CategoryApi implements CategoryApiInterface
{

    // ...

    /**
     * Implementation of CategoryApiInterface#delCategory
     */
    public function delCategory($categoryId, $apiKey = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **int**|  |
 **apiKey** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getCategoriesByFilter**
> Swagger\Server\Model\Category getCategoriesByFilter($filter)

Finds categories by filter

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CategoryApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CategoryApiInterface;

class CategoryApi implements CategoryApiInterface
{

    // ...

    /**
     * Implementation of CategoryApiInterface#getCategoriesByFilter
     */
    public function getCategoriesByFilter($filter)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**|  |

### Return type

[**Swagger\Server\Model\Category**](../Model/Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

