# Swagger\Server\Api\PaymentApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/and.Hromov/Music_Pasport/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentLicense**](PaymentApiInterface.md#paymentLicense) | **POST** /payment | payment license
[**setCard**](PaymentApiInterface.md#setCard) | **POST** /payment/setCard | Set card user


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.payment:
        class: Acme\MyBundle\Api\PaymentApi
        tags:
            - { name: "swagger_server.api", api: "payment" }
    # ...
```

## **paymentLicense**
> Swagger\Server\Model\Cheque paymentLicense($body)

payment license

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PaymentApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PaymentApiInterface;

class PaymentApi implements PaymentApiInterface
{

    // ...

    /**
     * Implementation of PaymentApiInterface#paymentLicense
     */
    public function paymentLicense(Cheque $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Cheque**](../Model/Cheque.md)|  |

### Return type

[**Swagger\Server\Model\Cheque**](../Model/Cheque.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **setCard**
> Swagger\Server\Model\Card setCard($body)

Set card user

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PaymentApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PaymentApiInterface;

class PaymentApi implements PaymentApiInterface
{

    // ...

    /**
     * Implementation of PaymentApiInterface#setCard
     */
    public function setCard(Card $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Card**](../Model/Card.md)|  |

### Return type

[**Swagger\Server\Model\Card**](../Model/Card.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

