# Cheque

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**number** | **int** |  | [optional] 
**card** | [**Swagger\Server\Model\Card**](Card.md) |  | [optional] 
**user** | [**Swagger\Server\Model\User**](User.md) |  | [optional] 
**license** | [**Swagger\Server\Model\License**](License.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


