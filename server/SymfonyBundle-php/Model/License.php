<?php
/**
 * License
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Server\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger Petstore
 *
 * This is a sample Petstore server.  You can find  out more about Swagger at  [http://swagger.io](http://swagger.io) or on  [irc.freenode.net, #swagger](http://swagger.io/irc/).
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@swagger.io
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Server\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the License model.
 *
 * @package Swagger\Server\Model
 * @author  Swagger Codegen team
 */
class License 
{
        /**
     * @var int|null
     * @SerializedName("id")
     * @Assert\Type("int")
     * @Type("int")
     */
    protected $id;

    /**
     * @var string|null
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $name;

    /**
     * @var string|null
     * @SerializedName("date_start")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $dateStart;

    /**
     * @var string|null
     * @SerializedName("date_end")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $dateEnd;

    /**
     * @var Swagger\Server\Model\User|null
     * @SerializedName("user")
     * @Assert\Type("Swagger\Server\Model\User")
     * @Type("Swagger\Server\Model\User")
     */
    protected $user;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->dateStart = isset($data['dateStart']) ? $data['dateStart'] : null;
        $this->dateEnd = isset($data['dateEnd']) ? $data['dateEnd'] : null;
        $this->user = isset($data['user']) ? $data['user'] : null;
    }

    /**
     * Gets id.
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id.
     *
     * @param int|null $id
     *
     * @return $this
     */
    public function setId($id = null)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets name.
     *
     * @param string|null $name
     *
     * @return $this
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets dateStart.
     *
     * @return string|null
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Sets dateStart.
     *
     * @param string|null $dateStart
     *
     * @return $this
     */
    public function setDateStart($dateStart = null)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Gets dateEnd.
     *
     * @return string|null
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Sets dateEnd.
     *
     * @param string|null $dateEnd
     *
     * @return $this
     */
    public function setDateEnd($dateEnd = null)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Gets user.
     *
     * @return Swagger\Server\Model\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets user.
     *
     * @param Swagger\Server\Model\User|null $user
     *
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}


