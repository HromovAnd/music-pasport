<?php
/**
 * UserApiInterface
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Server
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger Petstore
 *
 * This is a sample Petstore server.  You can find  out more about Swagger at  [http://swagger.io](http://swagger.io) or on  [irc.freenode.net, #swagger](http://swagger.io/irc/).
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@swagger.io
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Server\Api;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Swagger\Server\Model\ApiResponse;
use Swagger\Server\Model\User;

/**
 * UserApiInterface Interface Doc Comment
 *
 * @category Interface
 * @package  Swagger\Server\Api
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
interface UserApiInterface
{

    /**
     * Operation getUserByUsername
     *
     * Get user by user name
     *
     * @param  string $username   (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return Swagger\Server\Model\User[]
     *
     */
    public function getUserByUsername($username, &$responseCode, array &$responseHeaders);

    /**
     * Operation updateUserEmail
     *
     * Updated user
     *
     * @param  string $email  email that need to be updated (required)
     * @param  Swagger\Server\Model\User $body  Updated user object (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return void
     *
     */
    public function updateUserEmail($email, User $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation updateUserPas
     *
     * Updated user
     *
     * @param  string $password  pass that need to be updated (required)
     * @param  Swagger\Server\Model\User $body  Updated user object (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return void
     *
     */
    public function updateUserPas($password, User $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation updateUsername
     *
     * Updated user
     *
     * @param  string $username  name that need to be updated (required)
     * @param  Swagger\Server\Model\User $body  Updated user object (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return void
     *
     */
    public function updateUsername($username, User $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation uploadAvatar
     *
     * uploads an avatar image
     *
     * @param  int $userId  ID of user to update (required)
     * @param  string $additionalMetadata  Additional data to pass to server (optional)
     * @param  UploadedFile $file  file to upload (optional)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return Swagger\Server\Model\ApiResponse[]
     *
     */
    public function uploadAvatar($userId, $additionalMetadata = null, UploadedFile $file = null, &$responseCode, array &$responseHeaders);

    /**
     * Operation userLogin
     *
     * Logs user into the system
     *
     * @param  string $username  The user name for login (required)
     * @param  string $password  The password for login in clear text (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return string
     *
     */
    public function userLogin($username, $password, &$responseCode, array &$responseHeaders);

    /**
     * Operation userLogout
     *
     * Logs out current logged in user session
     *
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return void
     *
     */
    public function userLogout(&$responseCode, array &$responseHeaders);

    /**
     * Operation userSignUp
     *
     * Register user
     *
     * @param  Swagger\Server\Model\User $body  Created user object (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return Swagger\Server\Model\User[]
     *
     */
    public function userSignUp(User $body, &$responseCode, array &$responseHeaders);
}
