<?php
/**
 * AlbumApiInterface
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Server
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger Petstore
 *
 * This is a sample Petstore server.  You can find  out more about Swagger at  [http://swagger.io](http://swagger.io) or on  [irc.freenode.net, #swagger](http://swagger.io/irc/).
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@swagger.io
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Server\Api;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Swagger\Server\Model\Album;

/**
 * AlbumApiInterface Interface Doc Comment
 *
 * @category Interface
 * @package  Swagger\Server\Api
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
interface AlbumApiInterface
{

    /**
     * Operation createAlbum
     *
     * Add a new album
     *
     * @param  Swagger\Server\Model\Album $body   (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return Swagger\Server\Model\Album[]
     *
     */
    public function createAlbum(Album $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation delAlbum
     *
     * Deletes a album
     *
     * @param  int $albumId   (required)
     * @param  string $apiKey   (optional)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return void
     *
     */
    public function delAlbum($albumId, $apiKey = null, &$responseCode, array &$responseHeaders);

    /**
     * Operation getAlbumsByCategory
     *
     * Finds albums by category
     *
     * @param  string $category   (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return Swagger\Server\Model\Album[]
     *
     */
    public function getAlbumsByCategory($category, &$responseCode, array &$responseHeaders);
}
