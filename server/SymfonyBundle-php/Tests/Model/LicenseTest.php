<?php
/**
 * LicenseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger Petstore
 *
 * This is a sample Petstore server.  You can find  out more about Swagger at  [http://swagger.io](http://swagger.io) or on  [irc.freenode.net, #swagger](http://swagger.io/irc/).
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@swagger.io
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Server\Model;

/**
 * LicenseTest Class Doc Comment
 *
 * @category    Class */
// * @description License
/**
 * @package     Swagger\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LicenseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "License"
     */
    public function testLicense()
    {
        $testLicense = new License();
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "dateStart"
     */
    public function testPropertyDateStart()
    {
    }

    /**
     * Test attribute "dateEnd"
     */
    public function testPropertyDateEnd()
    {
    }

    /**
     * Test attribute "user"
     */
    public function testPropertyUser()
    {
    }
}
